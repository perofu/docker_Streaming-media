#build sshd and nginx from min install
FROM centos:centos6
#MAINTAINER perofu <perofu.com@gmail.com>
RUN yum install openssh-server tar gcc gcc-c++  libxml2 libxml2-devel zlib zlib-devel glibc glibc-devel glib2 glib2-devel krb5-libs krb5-devel openssl openssl-devel wget -y

RUN useradd -s /sbin/nologin www && cd /tmp/ && wget http://nginx.org/download/nginx-1.1.0.tar.gz && wget http://exim.mirror.fr/pcre/pcre-8.12.tar.gz  && wget http://h264.code-shop.com/download/nginx_mod_h264_streaming-2.2.7.tar.gz   && wget http://ncu.dl.sourceforge.net/project/yamdi/yamdi/1.9/yamdi-1.9.tar.gz

RUN tar -zxf /tmp/pcre-8.12.tar.gz -C /usr/local/src/ && cd /usr/local/src/pcre-8.12/ && ./configure && make && make install && tar -zxf /tmp/nginx_mod_h264_streaming-2.2.7.tar.gz -C /usr/local/src/ && sed -i '/zero_in_uri/,+3d' /usr/local/src/nginx_mod_h264_streaming-2.2.7/src/ngx_http_streaming_module.c && tar -xzf /tmp/nginx-1.1.0.tar.gz -C /usr/local/src/ && cd /usr/local/src/nginx-1.1.0/ && ./configure --user=www --group=www --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module --with-http_flv_module  --add-module=../nginx_mod_h264_streaming-2.2.7/   && make && make install  && tar -zxf /tmp/yamdi-1.9.tar.gz -C /usr/local/src/ && cd /usr/local/src/yamdi-1.9/  && make && make install

RUN mkdir -p /data/www/{logs,www} && chmod +w /data/www/logs && chown -R www:www /data/www && mkdir -p /usr/local/nginx/conf/vhost && echo '/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf' >> /root/nginx_start && echo '/usr/local/nginx/sbin/nginx -t' > /root/nginx_reload && echo 'kill -HUP `cat /usr/local/nginx/logs/nginx.pid`' >> /root/nginx_reload && chmod 700 /root/nginx_* && cd /usr/local/nginx/conf/ && mv nginx.conf nginx.conf.bak && wget -O nginx.conf 'http://git.oschina.net/perofu/docker_Streaming-media/blob/master/nginx.conf?dir=0&filepath=nginx.conf&oid=bd724f79642fafa99c6b9409a3dce3298402cab4&sha=b44d970eb1252d2e204c44631f4e0c4746eadca6' && ln -s /usr/local/nginx/sbin/nginx /usr/sbin/ && cd /data/www/www && wget -O player.swf 'http://files.git.oschina.net/group1/M00/00/93/fMqNk1XCzQWAG9EMAAFjorzeBPI374.swf?token=2283397eb8dd9e204083512797ba23e0&ts=1438830121'  && wget -O aa.mp4 'http://git.oschina.net/perofu/docker_Streaming-media/attach_files/download?i=14558&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2F93%2FfMqNk1XC0AeASwr0AEyMlPBnV24163.mp4%3Ftoken%3D34062dfcda102e2c0a68145f4b0348ec%26ts%3D1438830552'

RUN echo '#! /bin/bash' > /root/run.sh && echo '/usr/local/nginx/sbin/nginx' >> /root/run.sh && echo 'while :' >> /root/run.sh && echo 'do' >> /root/run.sh && echo ':' >> /root/run.sh  && echo 'done' >> /root/run.sh && chmod 777 /root/run.sh

RUN sed -i 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config && sed -i 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config && echo -e "/usr/sbin/nginx \n/usr/sbin/sshd -D" >> /etc/rc.local  && ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key && ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key && mkdir /var/run/sshd

EXPOSE 80

CMD ["/root/run.sh"]